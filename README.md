# woodblockpuzzleIA

### Étapes pour mettre en place l'environnement pour l'IA et son lancement

1. Prérequis : avoir java 8 (JDK 8) installé sur son poste et le configurer (si pas fait par défaut) par la suite dans le build path de Spring Tool Suite.
2. Cloner le projet GIT sur votre ordinateur.
3. Télécharger SpringToolSuite 4 ([STS 4](https://spring.io/tools)) et l'installer
4. Lancer SpringToolSuite puis :
    1.  Cliquer sur **File > Import**
    2.  Cliquer sur **Maven > Existing Maven Projects**
    3.  Au niveau du **root directory**, cliquer sur **Browse** puis sélectionner le dossier cloné précédemment. Le Pom.xml du projet sera détecté.
    4.  Valider le tout et attendre la fin de l'import du projet ainsi que le téléchargement des dépendances.
    5.  Pour le lancer le projet ou plutôt l'application, clique droit sur le projet puis ** Run As.. > Spring Boot App**
    6.  L'application sera accessible par défaut à l'adresse suivante : http://localhost:8080
    7.  Pour relancer l'application il suffira de cliquer sur la petite flèche blanche dans le petit rond vert au niveau de la barre d'outils.
5. Pour ajouter des dépendances, il faut le faire via le pom.xml
6. Git Stagging est disponible sous STS.
7. Pour déployer l'application et l'avoir sous forme d'archive **jar** : 
    1.  Clique droit sur le projet puis **Run As.. > Maven install**. Cela créera un jar dont la localisation est indiquée dans la console.
    2.  Une fois en possession du jar, la commande **java -jar *nomdelappli.jar*** lancera l'application.


