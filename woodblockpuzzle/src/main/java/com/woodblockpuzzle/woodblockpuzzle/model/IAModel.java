package com.woodblockpuzzle.woodblockpuzzle.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.woodblockpuzzle.woodblockpuzzle.Logger;
import com.woodblockpuzzle.woodblockpuzzle.controller.IAController;
import com.woodblockpuzzle.woodblockpuzzle.controller.interfaces.InterfaceResponse;
import com.woodblockpuzzle.woodblockpuzzle.entities.Carte;
import com.woodblockpuzzle.woodblockpuzzle.entities.Enchainement;
import com.woodblockpuzzle.woodblockpuzzle.entities.Piece;
import com.woodblockpuzzle.woodblockpuzzle.entities.PieceJSON;
import com.woodblockpuzzle.woodblockpuzzle.entities.Position;
import com.woodblockpuzzle.woodblockpuzzle.entities.solution.FinPartie;
import com.woodblockpuzzle.woodblockpuzzle.entities.solution.Solution;

public class IAModel {
	/* ********************************
	 * FIELDS
	 **********************************/
	static int method = 2;
	public static LinkedHashMap<String, Piece> pieces = null;
	static Carte carte = new Carte(8,8);
	static int globalScore = 0;	
	//solutions
	static ArrayList<Solution> solutions = new ArrayList<>();
	static Solution bestSolution;
	/* **************************
	 * METHODS
	 ****************************/
	/**
	 * SET evaluation method.
	 * @param method
	 * @return
	 */
	public static boolean setEvaluationMethod(final int method) {
		if(method<4 && method>0) {
			IAModel.method = method;
			return true;
		}
		return false;
	}
	/**
	 * RESET the map
	 * @return
	 */
	public static boolean resetCarte() {
		Logger.i("Carte reset");
		carte = new Carte(8,8);
		return true;
	}
	
	/* *******************************
	 * IA METHODS
	 *********************************/
	/**
	 * PLACE the {@link Piece} in the {@link Carte}, destroy the lines if there some to destroy
	 * and return the number of points associate to this move.
	 * @param carte
	 * @param position
	 * @param piece
	 * @return
	 */
	private static int placeAndDestroy(Carte carte, Position position, Piece piece) {
		carte.place(position.X, position.Y, piece);									//placer la piece dans la carte
		int destroyedLines = carte.destroyCompletedLines();							//supprimer les lignes pleines
		int points = piece.blocksCount();											//compter les points pour la piece placée. 
		if(destroyedLines>0) {
			points+=(destroyedLines*2-1)*80;										//compter les points pour les lignes detruites
		}
		Logger.d("blocks : "+piece.blocksCount());
		Logger.d("ligne detruite : "+destroyedLines);
		Logger.d("total : "+points);
		return points;
	}
	/**
	 * Take FIRST suitable position for the three piece in given order.
	 * @param carte
	 * @param p1
	 * @param p2
	 * @param p3
	 * @return
	 */
	public static InterfaceResponse takeFirstSolution(Piece p1, Piece p2, Piece p3) {
		Logger.i("----TAKE FISRT SOLUTION");
		Enchainement[] enchainements = new Enchainement[6];						//les diffÃ©rents enchainements possible avec les trois pieces
		enchainements[0] = new Enchainement(p1, p2, p3);
		enchainements[1] = new Enchainement(p1, p3, p2);
		enchainements[2] = new Enchainement(p2, p1, p3);
		enchainements[3] = new Enchainement(p2, p3, p1);
		enchainements[4] = new Enchainement(p3, p2, p1);
		enchainements[5] = new Enchainement(p3, p2, p2);
		
		for(Enchainement enchainement : enchainements) {
			//PIECE 1
			Carte carte1 = new Carte(carte);									//new Carte
			Position po1 = carte1.findFirstPosition(enchainement.p1);			//find position
			if(po1!=null) {
				int gamesPoints = placeAndDestroy(carte1, po1, enchainement.p1);//place p1
				//PIECE 2
				Carte carte2 = new Carte(carte1);									//new Carte
				Position po2 = carte2.findFirstPosition(enchainement.p2);			//find position
				if(po2!=null) {
					gamesPoints += placeAndDestroy(carte2, po2, enchainement.p2);	//place p2
					//PIECE 3
					Carte carte3 = new Carte(carte2);									//new Carte
					Position po3 = carte3.findFirstPosition(enchainement.p3);			//find position
					if(po3!=null) {
						gamesPoints += placeAndDestroy(carte3, po3, enchainement.p3);	//place p3
						carte = new Carte(carte3);										//update Carte to play
						return new Solution(											//return solution
								enchainement.p1, enchainement.p2, enchainement.p3,
								po1, po2, po3, gamesPoints);
					} else {														//p3 cannot be placed
						continue;
					}
				} else {														//p2 cannot be placed
					continue;
				}
			} else {													//p1 cannot be placed
				continue;
			}
		}
		return new FinPartie();										//NO SOLUTION
	}
	/**
	 * ANALYZE the {@link Carte} and {@link Piece}s. Find the best solution. 
	 * @param carte
	 * @param p1
	 * @param p2
	 * @param p3
	 * @return
	 */
	public static InterfaceResponse analyzeSolutions(Piece p1, Piece p2, Piece p3) {
		Logger.i("-----ANALYSE SOLUTIONS");
		Enchainement[] enchainements = new Enchainement[6];						//les diffÃ©rents enchainements possible avec les trois pieces
		enchainements[0] = new Enchainement(p1, p2, p3);
		enchainements[1] = new Enchainement(p1, p3, p2);
		enchainements[2] = new Enchainement(p2, p1, p3);
		enchainements[3] = new Enchainement(p2, p3, p1);
		enchainements[4] = new Enchainement(p3, p2, p1);
		enchainements[5] = new Enchainement(p3, p2, p2);
		
		int feuille = 0;
		int prune = 0;
		bestSolution = new Solution(null, null, null, null, 0);
		Carte bestCarte = null;
		for(Enchainement enchainement : enchainements) {						//pour chaque enchainement possible
			Logger.d("----------------ENCHAINEMENT");
			Logger.d(enchainement);
			ArrayList<Position> positionsP1 = 											//positions possibles pour p1 dans c
					carte.findSuitablePositions(enchainement.p1);
			if(positionsP1.size()>0) {				
				for(Position pos1 : positionsP1) {										//pour chaque position
					Logger.d("\t"+pos1);
					Carte carte1 = new Carte(carte);										//nouvelle carte c1
					int scoreP1 = placeAndDestroy(carte1, pos1, enchainement.p1);			//place p1 dans c1, detruit les lignes et compte les points
					
					ArrayList<Position> positionsP2 =										//positions possibles pour p2 dans c1
							carte1.findSuitablePositions(enchainement.p2);
					if(positionsP2.size()>0) {
						for(Position pos2 : positionsP2) {									//pour chaque position p2
							Logger.d("\t\t"+pos2);
							Carte carte2 = new Carte(carte1);									//nouvelle carte c2
							int scoreP2 = placeAndDestroy(carte2, pos2, enchainement.p2);		//place p2 dans c2, detruit les lignes et compte les points
							
							ArrayList<Position> positionsP3 =									//positions possibles pour p3 dans c2
									carte2.findSuitablePositions(enchainement.p3);
							if(positionsP3.size()>0) {
								for(Position pos3 : positionsP3) {								//pour chaque position p3
									Logger.d("\t\t\t"+pos3);
									Carte carte3 = new Carte(carte2);								//nouvelle carte c3
									int scoreP3 = placeAndDestroy(carte3, pos3, enchainement.p3);	//place p3 dans c3, detruit les lignes et compte les points
									
									int points = scoreP1+scoreP2+scoreP3;										//points gagnés sur cette solution
									Solution solution = new Solution(enchainement, pos1, pos2, pos3, points);	//solution trouvée
									solution.IAevaluation = evaluate(carte3, solution, points);					//évaluation de la solution
									solutions.add(solution);													//ajouter la solution à la liste des solutions.
									if(bestSolution.IAevaluation<solution.IAevaluation) {
										bestSolution = new Solution(solution);
										bestCarte = carte3;
									}
									feuille+=1;
									Logger.d("\t\t\tVALUE:"+solution.IAevaluation);
								}
							} else {
								prune+=1;
								Logger.d("\t\tp3 ne peut pas etre placée");
								continue;	
							}//if positions p3
						}
					} else {
						prune+=1;
						Logger.d("\tp2 ne peut pas etre placée");
						continue;
					}//if positions p2
				}
			} else {
				prune+=1;
				Logger.d("P1 ne peut pas etre placée");
			}//if positions p1
		}
		Logger.i("FEUILLES : "+feuille);
		Logger.i("PRUNES : "+prune);
		
		if(bestSolution.isNullSolution()) {
			return new FinPartie();
		}
		else {
			carte = bestCarte;
			return bestSolution;
		}
	}
	
	
	public static int evaluate(Carte carte, Solution solution, int score) {
		switch(method) {
		case 2: return evaluate2(carte);
		case 3: return evaluate3(score);
		default : return evaluate1(carte);
		}
	}
	/**
	 * Count the number of remaining holes in the {@link Carte}.
	 * Higher this number is the better it is.
	 * @param carte
	 * @return
	 */
	private static int evaluate1(Carte carte) {
		return carte.countEmptyBlocks();
	}
	/**
	 * Pour chaque piece existante, compte le nombre de positions possibles dans la carte et multiplie par la difficuté de la pièce.
	 * Ex : piece1.difficulté * 3 + ... + piece4.difficulté * 0 + ...
	 * @param carte
	 * @param solution
	 * @return
	 */
	private static int evaluate2(Carte carte) {
		int evaluation = 0;
		for(Entry<String, Piece> entry : pieces.entrySet()) {
			Piece piece = entry.getValue();
			ArrayList<Position> positions = carte.findSuitablePositions(piece);
			evaluation+=positions.size()*piece.get_difficulty();
		}
		return evaluation;
	}
	
	/**
	 * Just return the score
	 * @return
	 */
	private static int evaluate3(int score) {
		return score;
	}
	
	
	public static InterfaceResponse findSolution(String strP1, String strP2, String strP3) {
		//RETRIEVE PIECES
		if(pieces==null || pieces.size()<=0) {
			loadPieces();
		}
		//RETRIEVE PIECES TO PLAY
		Piece p1 = pieces.get(strP1);
		Piece p2 = pieces.get(strP2);
		Piece p3 = pieces.get(strP3);
		if(p1==null || p2==null || p3==null) {
			Logger.d("Erreur while retrieving Pieces to play");
			return null;
		}
		//
		//IA START
		Logger.d(
			  "\n------------------------------------"
			+ "\n----		IA		----"
			+ "\n\tPieces : "+p1.get_name()+", "+p2.get_name()+", "+p3.get_name()
			+ "\n------------------------------------");
		InterfaceResponse response = null;
		if(carte.countBlocks()<=16) {															//if carte has <=32 blocks then take the first suitable solution
			response = takeFirstSolution(p1, p2, p3);
		} else {
			long timeStart = System.currentTimeMillis();										//else analyze all the suitable solutions in order to find the best
			response = analyzeSolutions(p1, p2, p3);
			long timeStop = System.currentTimeMillis();
			Logger.d("Start time : "+timeStart);
			Logger.d("Stop time : "+timeStop);
			Logger.d("Ellapsed time : "+(timeStop-timeStart));
		}
		Logger.i(carte);
		return response;
	}
	
	private static void loadPieces() {
		if(pieces==null || pieces.size()<=0) {
			try {
				Logger.i("---------------------------"
						+ "\n---Chargement des pièces");
				String jsonString = IAController.loadPiecesJSON();
				if(jsonString==null) {return;}
				//PARSER/MAPPER JSON
				ObjectMapper mapper = new ObjectMapper();
				List<PieceJSON> pieces = mapper.readValue(jsonString, new TypeReference<List<PieceJSON>>() {});
				//CONSTRUIRE LA LISTE DES PIECES
				LinkedHashMap<String,Piece> lmapPieces = new LinkedHashMap<>();
				for(int i=0;i<pieces.size();i++) {
					Piece piece = new Piece(pieces.get(i));
					lmapPieces.put(piece.get_name(), piece);
				}
				IAModel.pieces = lmapPieces;
				Logger.i("---------------------------"
						+ "\n---Chargement terminé");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return;
	}
	
}