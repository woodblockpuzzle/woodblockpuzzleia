package com.woodblockpuzzle.woodblockpuzzle;

public class Logger {
	public static int level = 1;
	public static int LEVEL_OFF = 2;
	public static int LEVEL_INFO = 1;
	public static int LEVEL_DEBUG = 0;
	public static void d(Object msg) {
		if(level<=LEVEL_DEBUG) {
			System.out.println("--DEBUG-- "+msg);
		}
	}
	public static void i(Object msg) {
		if(level<=LEVEL_INFO) {
			System.out.println("--INFO-- "+msg);
		}
	}
}
