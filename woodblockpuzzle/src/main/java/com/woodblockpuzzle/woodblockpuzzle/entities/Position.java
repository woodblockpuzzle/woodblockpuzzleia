package com.woodblockpuzzle.woodblockpuzzle.entities;

public class Position {
	public int X;
	public int Y;
	public Position(int x, int y) {
		this.X=x;
		this.Y=y;
	}
	@Override
	public String toString() {
		return "("+X+","+Y+")";
	}
}
