package com.woodblockpuzzle.woodblockpuzzle.entities;

/**
 * Classe qui correspond au format JSON des pièces tel que défini sur le serveur Angular.
 * @author Louis
 *
 */
public class PieceJSON {
	public String nom;
	public int[][] values;
}
