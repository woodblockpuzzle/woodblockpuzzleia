package com.woodblockpuzzle.woodblockpuzzle.entities.solution;

import com.woodblockpuzzle.woodblockpuzzle.controller.interfaces.InterfaceResponse;

public class FinPartie implements InterfaceResponse{
	public String toString() {
		return "{ \"status\" : \"end\" }";
	}
}
