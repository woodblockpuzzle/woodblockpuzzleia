package com.woodblockpuzzle.woodblockpuzzle.entities;

public class Enchainement{
	/* *************************
	 * FIELDS
	 ***************************/
	public final Piece p1;
	public final Piece p2;
	public final Piece p3;
	/* *************************
	 * CONSTRUCTOR
	 ***************************/
	public Enchainement(final Piece p1, final Piece p2, final Piece p3) {
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
	}
	public final Enchainement copy() {
		return new Enchainement(this.p1, this.p2, this.p3);
	}
	/* *************************
	 * METHODS
	 ***************************/
	@Override
	public String toString() {
		return "p1: \n"+p1
				+"\np2: \n"+p2
				+"\np3: \n"+p3;
	}
}
