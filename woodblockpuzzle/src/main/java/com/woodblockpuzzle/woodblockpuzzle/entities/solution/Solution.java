package com.woodblockpuzzle.woodblockpuzzle.entities.solution;

import com.woodblockpuzzle.woodblockpuzzle.controller.interfaces.InterfaceResponse;
import com.woodblockpuzzle.woodblockpuzzle.entities.Enchainement;
import com.woodblockpuzzle.woodblockpuzzle.entities.Piece;
import com.woodblockpuzzle.woodblockpuzzle.entities.Position;

public class Solution implements InterfaceResponse{
	/* ****************
	 * FIELDS
	 ******************/
	public final Enchainement enchainement;
	public final Position p1;
	public final Position p2;
	public final Position p3;
	public int IAevaluation;
	public int gamePoints;
	/* ****************
	 * CONSTRUCTOR
	 ******************/
	public Solution(final Enchainement enchainement, final Position p1, final Position p2, final Position p3, int gamePoints) {
		this.enchainement = enchainement;
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
		this.gamePoints = gamePoints;
		IAevaluation = Integer.MIN_VALUE;
	}
	public Solution(final Piece pi1, final Piece pi2, final Piece pi3, final Position p1, final Position p2, final Position p3, int gamePoints) {
		this.enchainement = new Enchainement(pi1, pi2, pi3);
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
		this.gamePoints = gamePoints;
		IAevaluation = Integer.MIN_VALUE;
	}
	public Solution(final Solution solution) {
		this.enchainement = new Enchainement(solution.enchainement.p1, solution.enchainement.p2, solution.enchainement.p3);
		this.p1 = solution.p1;
		this.p2 = solution.p2;
		this.p3 = solution.p3;
		this.gamePoints = solution.gamePoints;
		this.IAevaluation = solution.IAevaluation;
		
	}
	/* ****************
	 * METHODS
	 ******************/
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append("\"status\" : \"run\",");
		sb.append("\"points\" : \""+gamePoints+"\",");
		System.out.println("gamePoints : "+gamePoints);
		sb.append("\"pieces\" : [ ");
			sb.append("{ \"nom\": \""+enchainement.p1.get_name()+"\",");
			sb.append(" \"x\" : \""+p1.X+"\",");
			sb.append(" \"y\" : \""+p1.Y+"\" },");
			sb.append("{ \"nom\": \""+enchainement.p2.get_name()+"\",");
			sb.append(" \"x\" : \""+p2.X+"\",");
			sb.append(" \"y\" : \""+p2.Y+"\" },");
			sb.append("{ \"nom\": \""+enchainement.p3.get_name()+"\",");
			sb.append(" \"x\" : \""+p3.X+"\",");
			sb.append(" \"y\" : \""+p3.Y+"\" }");
		sb.append("]}");
		return sb.toString();
	}
	/**
	 * @return <code>true</code> if solution is a null solution.
	 */
	public boolean isNullSolution() {
		if(this.p1==null || this.p2==null || this.p3==null) {
			return true;
		}
		return false;
	}
}
