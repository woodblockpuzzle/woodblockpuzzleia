package com.woodblockpuzzle.woodblockpuzzle.entities;

import java.util.ArrayList;

public class Carte{
	/* ***************************
	 * FIELDS
	 *****************************/
	protected int _row;
	protected int _col;
	protected int[][] _points;
	/* ***************************
	 * CONSTRUCTOR
	 *****************************/
	public Carte(int row, int col, final int[][] points) {
		this._row = row;
		this._col = col;
		_points = new int[row][col];
		for(int i=0;i<_row;i++) {
			for(int j=0;j<_col;j++) {
				_points[i][j]=points[i][j];
			}
		}
	}
	public Carte(int row, int col) {
		this._row = row;
		this._col = col;
		this._points = new int[row][col];
		for(int i=0;i<this._row;i++) {
			for(int j=0;j<this._col;j++) {
				_points[i][j]=0;
			}
		}
	}
	public Carte(int row, int col, final int...points) {
		this._row = row;
		this._col = col;
		_points = new int[row][col];
		for(int i=0;i<_row;i++) {
			for(int j=0;j<_col;j++) {
				_points[i][j]=points[i*col+j];
			}
		}
	}
	public Carte(Carte carte) {
		this._row =carte._row;
		this._col = carte._col;
		this._points = new int[_row][_col];
		for(int i=0;i<_row;i++) {
			for(int j=0;j<_col;j++) {
				_points[i][j]=carte.value(i,j);
			}
		}
	}
	/**
	 * Get a short DESCRIPTION of the {@link Carte}
	 */
	@Override
	public String toString() {
		String desc = this.getClass().getSimpleName();
		desc+=" - row:"+_row+", col:"+_col;
		for(int[] row : _points) {
			desc+="\n";
			for(int val : row) {
				desc+=" "+val;
			}
		}
		return desc;
	}
	/* ***************************
	 * METHODS
	 *****************************/
	/**
	 * COUNT number of blocks contained in this {@link Carte}
	 * @return
	 */
	public int countBlocks() {
		int count = 0;
		for(int i=0;i<this._row;i++) {
			for(int j=0;j<this._col;j++) {
				count+=this._points[i][j];
			}
		}
		return count;
	}
	/**
	 * COUNT number of empty blocks in this {@link Carte}
	 * @return
	 */
	public int countEmptyBlocks() {
		return this._row*this._col - countBlocks();
	}
	/**
	 * PLACE a {@link Piece} in the {@link Carte} at specified row and col.
	 * WARNING -> no verifications are made here.
	 * @param row
	 * @param col
	 * @param piece
	 * @return
	 */
	public void place(int row, int col, final Piece piece) {
		for(int i=0;i<piece.rowCount();i++) {
			for(int j=0;j<piece.columnCount();j++) {
				if(piece.value(i, j)==1) {
					this._points[row+i][col+j] = piece._points[i][j];
				}
			}
		}
	}
	/**
	 * Return FIRST suitable position for the given {@link Piece} in this {@link Carte}.
	 * @param piece
	 * @return
	 */
	public Position findFirstPosition(Piece piece) {
		for(int i=0;i<this._row;i++) {
			for(int j=0;j<this._col;j++) {
				if(this.isSuitablePosition(i, j, piece)) {					//suitable position for the piece
					return new Position(i,j);									//return position
				}
			}
		}
		return null;														//no suitable position
	}
	/**
	 * Find suitable POSITIONS for the given {@link Piece} in this {@link Carte}
	 * @param plateau
	 * @param piece
	 * @return
	 */
	public ArrayList<Position> findSuitablePositions(Piece piece) {
		ArrayList<Position> points = new ArrayList<>();
		for(int i=0;i<this.rowCount();i++) {
			for(int j=0;j<this.colCount();j++) {
				if(this.isSuitablePosition( i, j, piece)) {					//suitable position for the piece
					points.add(new Position(i,  j));							//add result to the list of suitable positions
				}
			}
		}
		return points;
	}
	
	
	/**
	 * Check if {@link Position}(row, col) is a SUITABLE position for the given {@link Piece} in this {@link Carte}
	 * @param map
	 * @param row
	 * @param col
	 * @param piece
	 * @return
	 */
	private boolean isSuitablePosition(int row, int col, Piece piece) {
		//Checks if piece does not go out of bounds of the Carte 
		if(  (row+piece.rowCount())>this.rowCount() || (col+piece.columnCount())>this.colCount()    ) {
			return false;
		}
		//Check if piece does not collide with this Carte's block.
		//for each row of the piece
		for(int i= 0;i<piece.rowCount();i++) {
			//for each column of the piece
			for(int j=0;j<piece.columnCount();j++) {
				//check collision with the carte content
				if(piece.value(i, j)==1 && this.value(row+i, col+j)==1) {			//collision between piece and this carte's blocks
					return false;
				}
			}
		}
		return true;																//if no collision, then it's a suitable position
	}
	
	
	/**
	 * Analyze specified interval of rows and columns, destroy completed lines and return number of destroyed lines.
	 * @param rowStart
	 * @param rowStop
	 * @param colStart
	 * @param colStop
	 * @return
	 */
	@Deprecated
	public int destroyCompletedLines(int rowStart, int rowCount, int colStart, int colCount) {
		//ANALYZE
		int[] rowSum = new int[this.rowCount()];
		int[] colSum = new int[this.colCount()];
		
		int rowMax = rowStart+rowCount;
		int colMax = colStart+colCount;
		//analyze rows
		for(int i=rowStart;i<rowMax;i++) {
			for(int j=0;j<this._col;j++) {
				rowSum[i]+=this._points[i][j];
			}
		}
		//analyze columns
		for(int j=colStart; j<colMax; j++) {
			for(int i=0;i<this._row;i++) {
				colSum[j]+=this._points[i][j];
			}
		}
		//DESTROY
		int totalDestoyedLines = 0;
		//rows
		for(int i=0;i<this._row;i++) {
			if(rowSum[i]==this._col) {
				totalDestoyedLines+=1;
				for(int j=0;j<this._col;j++) {
					this._points[i][j]=0;
				}
			}
		}
		//columns
		for(int j=0;j<this._col;j++) {
			if(colSum[j]==this._row) {
				totalDestoyedLines+=1;
				for(int i=0;i<this._row;i++) {
					this._points[i][j]=0;
				}
			}
		}
		return totalDestoyedLines;
	}
	
	/**
	 * Destroy completed lines.
	 * @return the number of destroyed lines.
	 */
	public int destroyCompletedLines() {
		//IDENTIFY completed lines
		int[] rowSum = new int[this.rowCount()];
		int[] colSum = new int[this.colCount()];
		for(int i=0; i<this.rowCount();i++) {
			for(int j=0; j<this.rowCount();j++) {
				rowSum[i]+=this.value(i, j);									//identify completed rows
				colSum[j]+=this.value(i, j);									//identify completed columns
			}
		}
		//DESTROY
		int totalDestroyedLines = 0;											//number of destroyed lines
		//COLUMN
		for(int j=0;j<colSum.length;j++) {										//destroy completed columns.
			if(colSum[j]==this.rowCount()) {										//i is a completed column
				totalDestroyedLines+=1;
				for(int i=0;i<this.rowCount();i++) {								//reset line's values to 0
					this._points[i][j]=0;
				}
			}
		}
		//ROWS
		for(int i=0;i<rowSum.length;i++) {										//destroy completed rows
			if(rowSum[i]==this.colCount()) {										//i is a completed row
				totalDestroyedLines+=1;
				for(int j=0;j<this.colCount();j++) {								//reset line's values to 0
					this._points[i][j]=0;
				}
			}
		}	
		return totalDestroyedLines;												//return number of destroyed lines
	}
	/* ***************************
	 * GETTERS & SETTERS
	 *****************************/
	/**
	 * Get number of row
	 * @return
	 */
	public final int rowCount() {
		return _row;
	}
	/**
	 * Get number of column
	 * @return
	 */
	public final int colCount() {
		return _col;
	}
	public final int value(int row, int col) {
		return _points[row][col];
	}	
}