package com.woodblockpuzzle.woodblockpuzzle.entities;

public class Piece {
	/* ***********************
	 * FIELDS
	 *************************/
	protected String _name;
	protected int[][] _points;
	protected int _blocksCount;
	protected int _row;
	protected int _col;
	/* ***********************
	 * CONSTRUCTOR
	 *************************/
	public Piece(String name, int row, int col, final int...points) {
		_name = name;
		_blocksCount = 0;
		_col = col;
		_row = row;
		_points = new int[row][col];
		for(int i=0;i<_row;i++) {
			for(int j=0;j<_col;j++) {
				_points[i][j]=points[i*_col+j];
				this._blocksCount+=_points[i][j];
			}
		}
	}
	public Piece(String name, int row, int col, final int[][] points) {
		_name =name;
		_blocksCount = 0;
		_col = col;
		_row = row;
		_points = new int[row][col];
		for(int i=0;i<_row;i++) {
			for(int j=0;j<_col;j++) {
				_points[i][j]=points[i][j];
				this._blocksCount+=_points[i][j];
			}
		}
	}
	public Piece(PieceJSON piecejson) {
		this._name = piecejson.nom;
		this._row = piecejson.values.length;
		this._col = piecejson.values[0].length;
		this._points = new int[_row][_col];
		this._blocksCount=0;
		for(int i=0;i<this._row;i++) {
			for(int j=0;j<this._col;j++) {
				this._points[i][j] = piecejson.values[i][j];
				this._blocksCount+=this._points[i][j];
			}
		}
	}
	/* ***********************
	 * METHODS
	 *************************/
	@Override
	public String toString() {
		String desc = this.getClass().getSimpleName();
		desc+=" - name:"+_name+", row:"+_row+", col:"+_col;
		for(int[] row : _points) {
			desc+="\n";
			for(int val : row) {
				desc+=" "+val;
			}
		}
		return desc;
	}
	/**
	 * Get value in tab.
	 * @param row
	 * @param col
	 * @return
	 */
	public int value(int row, int col) {
		return _points[row][col];
	}
	
	
	/* ******************
	 * Getters & Setters
	 ********************/
	public int blocksCount() {
		return this._blocksCount;
	}
	public int rowCount() {
		return this._row;
	}
	public int columnCount() {
		return this._col;
	}
	public final String get_name() {
		return _name;
	}
	public final int[][] get_points() {
		return _points;
	}
	public final int get_difficulty() {
		return _blocksCount;
	}
}
