package com.woodblockpuzzle.woodblockpuzzle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WoodblockpuzzleApplication {

	public static void main(String[] args) {
		SpringApplication.run(WoodblockpuzzleApplication.class, args);
	}

}
