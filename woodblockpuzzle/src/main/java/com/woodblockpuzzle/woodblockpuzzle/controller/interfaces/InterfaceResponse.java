package com.woodblockpuzzle.woodblockpuzzle.controller.interfaces;

public interface InterfaceResponse {
	public String toString();
}
