package com.woodblockpuzzle.woodblockpuzzle.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.woodblockpuzzle.woodblockpuzzle.Logger;
import com.woodblockpuzzle.woodblockpuzzle.controller.interfaces.InterfaceResponse;
import com.woodblockpuzzle.woodblockpuzzle.model.IAModel;

@RestController
public class IAController {
	/* **************************
	 * FIELDS
	 ****************************/
	static String piecesURL = "http://localhost:4200/api/pieces/pieces.json";
	/* **************************
	 * ROUTES
	 ****************************/
	@RequestMapping(value="/method", method=RequestMethod.POST)
	public String setMethod(@RequestParam Long method) {
		if(IAModel.setEvaluationMethod(method.intValue())) {
			return "OK";
		}
		return "KO";
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/compute", method=RequestMethod.GET)
	public String playIA(@RequestParam String p1, @RequestParam String p2, @RequestParam String p3) {
		Logger.d("Received. Pieces to play : P1="+p1+", P2="+p2+", P3="+p3);
		InterfaceResponse ir = IAModel.findSolution(p1, p2, p3);
		System.out.println(ir);
		return ir.toString();
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/reset")
	public String reset() {
		IAModel.resetCarte();
		return "";
	}
	/* ****************************
	 * METHODES
	 ******************************/
	public static String loadPiecesJSON() {
		if(IAModel.pieces == null || IAModel.pieces.size()<=0) {
			Logger.d("Chargement des pièces");
			try {
				//REQUETTE HTTP
				URL url = new URL(piecesURL);
				HttpURLConnection con = (HttpURLConnection) url.openConnection();
				con.setRequestMethod("GET");
				Logger.d("requete OK");
				//CONTENU 
				BufferedReader in = new BufferedReader(
						new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer content = new StringBuffer();
				while((inputLine = in.readLine())!=null) {
					content.append(inputLine);
				}
				Logger.d("contenu OK");
				return content.toString();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			Logger.d("chargement des pièces, erreur");
			return null;
		}
		Logger.d("Pieces déjà chargées");
		return null;
	}
}
